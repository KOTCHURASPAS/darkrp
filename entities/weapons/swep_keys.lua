if SERVER then
	AddCSLuaFile()
end

SWEP.Category              = "[URP] RP Stuff"
SWEP.PrintName             = "Ключи"
SWEP.Slot                  = 1
SWEP.SlotPos               = 1
SWEP.DrawAmmo              = false
SWEP.DrawCrosshair         = false

SWEP.Author                = "roni_sl"
SWEP.Instructions          = "ЛКМ - Открыть\nПКМ - Закрыть"
SWEP.Base                  = "sck_base"
DEFINE_BASECLASS( "sck_base" )

SWEP.Spawnable             = true
SWEP.Sound                 = "doors/door_latch3.wav"
SWEP.Primary.ClipSize      = -1
SWEP.Primary.DefaultClip   = 0
SWEP.Primary.Automatic     = false
SWEP.Primary.Ammo          = ""

SWEP.Secondary.ClipSize    = -1
SWEP.Secondary.DefaultClip = 0
SWEP.Secondary.Automatic   = false
SWEP.Secondary.Ammo        = ""

SWEP.HoldType              = "slam"
SWEP.ViewModelFOV          = 62.5
SWEP.ViewModelFlip         = true
SWEP.UseHands              = false
SWEP.ViewModel             = "models/weapons/c_slam.mdl"
SWEP.WorldModel            = "models/santosrp/key/car_key.mdl"
SWEP.ShowViewModel         = false
SWEP.ShowWorldModel        = false

SWEP.ViewModelBoneMods = {
	["Detonator"]                   = { scale = Vector(0.009, 0.009, 0.009), pos = Vector(0, 0, 0),   angle = Angle(0, 0, 0) },
	["ValveBiped.Bip01_R_UpperArm"] = { scale = Vector(1, 1, 1),             pos = Vector(0, 0, -10), angle = Angle(0, 0, 0) },
	["Slam_base"]                   = { scale = Vector(0.009, 0.009, 0.009), pos = Vector(0, 0, 0),   angle = Angle(0, 0, 0) }
}

SWEP.VElements = {
	["Keys"] = {
		type              = "Model",
		model             = "models/santosrp/key/car_key.mdl",
		bone              = "ValveBiped.Bip01_L_Hand",
		rel               = "",
		pos               = Vector(4.959, -1.933, 2.69),
		angle             = Angle(7.727, 7.168, 161.57),
		size              = Vector(1.21, 1.21, 1.21),
		color             = Color(255, 255, 255, 255),
		surpresslightning = false,
		material          = "",
		skin              = 0,
		bodygroup         = {}
	}
}
SWEP.WElements = {
	["Keys"] = {
		type              = "Model",
		model             = "models/santosrp/key/car_key.mdl",
		bone              = "ValveBiped.Bip01_R_Hand",
		rel               = "",
		pos               = Vector(7.637, 1.254, -0.427),
		angle             = Angle(0, 0, -75.82),
		size              = Vector(1.136, 1.136, 1.136),
		color             = Color(255, 255, 255, 255),
		surpresslightning = false,
		material          = "",
		skin              = 0,
		bodygroup         = {}
	}
}
SWEP.IronSightsPos = Vector(4.239, 0, -0.88)
SWEP.IronSightsAng = Vector(0, 0, 27.34)


function SWEP:Initialize()
	BaseClass.Initialize( self )
	self:SetHoldType( "slam" )
end

function SWEP:CanUse()
	local ent = self.Owner:GetEyeTrace().Entity
	if not ent or not ent:IsDoor() then
		urp.notify(self.Owner, 1, 4, "Вы должны смотреть на дверь!")
		return
	end
end

function SWEP:PrimaryAttack()
	self:SetNextPrimaryFire( CurTime() + 0.5 )
	if SERVER then
		local ent = self:CanUse()
		if not ent then return end

		urp.properties.close_door(self.Owner, ent)
	end
end

function SWEP:SecondaryAttack()
	self:SetNextSecondaryFire( CurTime() + 0.5 )
	if SERVER then
		local ent = self:CanUse()
		if not ent then return end

		urp.properties.open_door(self.Owner, ent)
	end
end
