if SERVER then
	AddCSLuaFile()
	util.AddNetworkString("swep_admin_doors.sync")
end

SWEP.Category            = "[URP] Admin Stuff"
SWEP.PrintName           = "Door configurator"
SWEP.Slot                = 1
SWEP.SlotPos             = 1
SWEP.DrawAmmo            = false
SWEP.DrawCrosshair       = true

SWEP.Author              = "roni_sl"
SWEP.Instructions        = "LMB: Link door\nRMB: Open VGUI"

SWEP.WorldModel          = ""

SWEP.ViewModelFOV        = 62
SWEP.ViewModelFlip       = false
SWEP.UseHands            = true
SWEP.Spawnable           = true

SWEP.Primary.ClipSize    = -1
SWEP.Primary.DefaultClip = 0
SWEP.Primary.Automatic   = false
SWEP.Primary.Ammo        = ""
SWEP.Secondary           = SWEP.Primary

SWEP.DoorsInOrder        = {}

function SWEP:Initialize()
	self:SetHoldType("magic")
end

function SWEP:DrawHUD()
	for k,v in pairs(ents.GetAll()) do
		if not v:IsDoor() then continue end
		local pos = v:GetPos():ToScreen()

		pos.x = math.floor(pos.x)
		pos.y = math.floor(pos.y)

		local in_order = self.DoorsInOrder[v:EntIndex()] and true or false

		if v:GetPos():DistToSqr(LocalPlayer():GetPos()) < 200000 then
			if v:IsProperty() then
				local str = ("Name: %s, Cost: %s"):format(v:GetName(), urp.format_money(v:GetCost()))
				urp.util.DrawShadowText(str, urp.util.GetFont(10), pos.x, pos.y-20, Color(50,200,50), 1, 1)
			end
			if not in_order then
				urp.util.DrawShadowText("Door ID: " .. v:EntIndex(), urp.util.GetFont(10), pos.x, pos.y, color_white, 1, 1)
			end
		end

		if in_order then
			urp.util.DrawShadowText("Door ID: " .. v:EntIndex(), urp.util.GetFont(10), pos.x, pos.y, Color(200,200,50), 1, 1)
		end
	end
end

function SWEP:Deploy()
	if CLIENT or not IsValid(self:GetOwner()) then return true end
	self:GetOwner():DrawWorldModel(false)
	return true
end

function SWEP:Holster()
	return true
end

function SWEP:PreDrawViewModel()
	return true
end


function SWEP:PrimaryAttack()
	if not SERVER then return end
	self:SetNextPrimaryFire(CurTime() + 0.4)

	local ent = self.Owner:GetEyeTrace().Entity

	if not ent then return end
	if not ent:IsDoor() then return end
	if not self.Owner:IsSuperAdmin() then return end

	if ent:IsProperty() then
		urp.notify(self.Owner, 1, 4, "Дверь уже зарегистрирована!")
		return
	end

	local id = ent:EntIndex()

	if self.DoorsInOrder[id] then
		self.DoorsInOrder[id] = nil
	else
		self.DoorsInOrder[id] = ent
	end

	net.Start("swep_admin_doors.sync")
	net.WriteEntity(self)
	net.WriteTable(self.DoorsInOrder)
	net.Send(self.Owner)
end

function SWEP:SecondaryAttack()
	self:SetNextSecondaryFire(CurTime() + 0.3)
	if not CLIENT then return end

	if table.IsEmpty(self.DoorsInOrder) then return end
	urp.properties.open_creation_menu(self, self.DoorsInOrder)
end

function SWEP:Reload()
	if not CLIENT then return end
end

if CLIENT then
	net.Receive("swep_admin_doors.sync", function()
		local ent = net.ReadEntity()
		ent.DoorsInOrder = net.ReadTable()
	end)
end
