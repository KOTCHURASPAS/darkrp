urp.properties = urp.properties or {}
urp.properties.doors = urp.properties.doors or {}
urp.properties.list = urp.properties.list or {}

local door_meta = FindMetaTable("Entity")

local door_classes = {
	["func_door_rotating"] = true,
	["prop_door_rotating"] = true,
	["func_movelinear"]    = true,
	["prop_dynamic"]       = false,
	["func_door"]          = true,
}

function door_meta:IsDoor()
	return door_classes[self:GetClass()]
end

function door_meta:GetPropertyTable()
	return urp.properties.list[urp.properties.doors[self:EntIndex()]]
end

function door_meta:IsProperty()
	return self:GetPropertyTable() and true or false
end

function door_meta:GetName()
	return self:GetPropertyTable().name
end

function door_meta:GetCost()
	return self:GetPropertyTable().cost
end

function door_meta:IsOwned()
	return self:GetPropertyTable().char_id and true or false
end

function door_meta:GetOwnerID()
	return self:GetPropertyTable().char_id
end
