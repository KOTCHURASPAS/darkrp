hook.Add("urp.database.loaded", "urp.properties", function()
	MySQLite.query(("SELECT * FROM urp_properties WHERE map = %s;"):format(
		MySQLite.SQLStr(string.lower(game.GetMap()))
	), function(data)
		if not data then return end

		for k,v in pairs(data) do
			urp.properties.register(tonumber(v.id), v.name, tonumber(v.cost), util.JSONToTable(v.doors), tonumber(v.char_id))
		end

	end, function(e) urp.print_debug("MYSQL", e) end)
end)

hook.Add("PlayerIsLoaded", "urp.properties.PlayerIsLoaded", function(ply)
	timer.Simple(10, function()
		urp.properties.sync(ply)
	end)
end)

function urp.properties.register(id, name, cost, doors, char_id, need_sync)
	print(id, name, cost, doors, char_id, need_sync)
	doors = urp.properties.get_ent_ids(doors)

	urp.properties.list[id] = {
		name = name,
		cost = cost,
		doors = doors,
		char_id = char_id
	}

	for index,_ in pairs(doors) do
		urp.properties.doors[index] = id
	end

	urp.print_debug("PROPERTY", ("%s (%d) is loaded!"):format(name, id))

	if need_sync then
		urp.properties.sync()
	end
end

util.AddNetworkString("urp.properties.sync")
function urp.properties.sync(ply)
	net.Start("urp.properties.sync")
	net.WriteTable(urp.properties.doors)
	net.WriteTable(urp.properties.list)
	if ply then
		net.Send(ply)
	else
		net.Broadcast()
	end
end

function urp.properties.get_map_creation_ids(arr)
	local to_return = {}

	for k,_ in pairs(arr) do
		local ent = ents.GetByIndex(k)
		if not ent then continue end

		to_return[ent:MapCreationID()] = true
	end

	return to_return
end

function urp.properties.get_ent_ids(arr)
	local to_return = {}

	for k,_ in pairs(arr) do
		local ent = ents.GetMapCreatedEntity(k)
		if not ent then continue end

		to_return[ent:EntIndex()] = true
	end

	return to_return
end

function urp.properties.is_name_taken(name, func)
	MySQLite.query(("SELECT id FROM urp_properties WHERE name = %s AND map = %s;"):format(
		MySQLite.SQLStr(name),
		MySQLite.SQLStr(string.lower(game.GetMap()))
	), function(data)
		func(data and true or false)
	end)
end

function urp.properties.add(ply, name, cost, doors)
	urp.properties.is_name_taken(name, function(is_taken)
		if is_taken then
			urp.notify(ply, 1, 4, "Название уже занято")
			return
		end

		doors = urp.properties.get_map_creation_ids(doors)

		MySQLite.query(([[
			INSERT INTO urp_properties(
				map, name, cost, doors
			) VALUES (
				%s, %s, %d, %s
			)
		]]):format(
			MySQLite.SQLStr(string.lower(game.GetMap())),
			MySQLite.SQLStr(name),
			cost,
			MySQLite.SQLStr(util.TableToJSON(doors))
		), function(data, index)
			if index then
				urp.properties.register(tonumber(index), name, cost, doors, nil, true)
			else
				MySQLite.query("SELECT last_insert_rowid() AS last_index FROM urp_properties LIMIT 1;", function(data)
					if not data then return end
					urp.properties.register(tonumber(data[1].last_index), name, cost, doors, nil, true)
				end)
			end

		end, function(e) urp.print_debug("MYSQL", e) end)
	end)
end

function urp.properties.has_access(ply, ent)
	if not ent:IsDoor() then return false end
	if ent:GetOwnerID() ~= ply.char_id then return false end
	if not ply:IsDev() then return false end

	return true
end

function urp.properties.open_door(ply, ent)
	if not urp.properties.has_access(ply, ent) then
		urp.notify(ply, 1, 4, "Ваши ключи неподходят к этой двери!")
		return
	end

	ent:Fire("Unlock")
	urp.notify(ply, 0, 4, "Вы открыли дверь!")
	ent:EmitSound( "doors/latchunlocked1.wav" )
end

function urp.properties.close_door(ply, ent)
	if not urp.properties.has_access(ply, ent) then
		urp.notify(ply, 1, 4, "Ваши ключи неподходят к этой двери!")
		return
	end

	ent:Fire("Lock")
	urp.notify(ply, 0, 4, "Вы закрыли дверь!")
	ent:EmitSound( "doors/default_locked.wav" )
end

function urp.properties.remove(id)
	MySQLite.query(([[
		DELETE FROM urp_properties WHERE id = %d;
	]]):format(id), function(data)
		urp.properties.list[id] = nil
		urp.properties.sync()
	end, function(e) urp.print_debug("MYSQL", e) end)
end

util.AddNetworkString("urp.properties.add")
net.Receive("urp.properties.add", function(_, ply)
	if not ply:IsSuperAdmin() then return end
	local name = net.ReadString()
	local cost = net.ReadInt(32)
	local ent = net.ReadEntity()

	local doors = ent.DoorsInOrder
	urp.properties.add(ply, name, cost, doors)

	ent.DoorsInOrder = {}
	net.Start("swep_admin_doors.sync")
	net.WriteEntity(ent)
	net.WriteTable({})
	net.Send(ply)
end)
