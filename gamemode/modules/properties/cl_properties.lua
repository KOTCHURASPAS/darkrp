net.Receive("urp.properties.sync", function()
	urp.properties.doors = net.ReadTable()
	urp.properties.list = net.ReadTable()
end)

function urp.properties.open_creation_menu(ent)
	if IsValid(f) then f:Remove() end
	f = vgui.Create("DFrame")
	f:SetTitle("Door managment")
	f:SetSize(250,125)
	f:Center()
	f:MakePopup()

	local te_n = vgui.Create("DTextEntry", f)
	te_n:SetSize(f:GetWide() - 10, 25)
	te_n:Dock(TOP)
	te_n:DockMargin(5,0,5,5)
	te_n:SetValue("Write name")

	local te = vgui.Create("DTextEntry", f)
	te:SetSize(f:GetWide()-10, 25)
	te:Dock(TOP)
	te:DockMargin(5,0,5,5)
	te:SetValue("Write cost")
	te:SetNumeric(true)

	local b = vgui.Create("DButton", f)
	b:SetSize(f:GetWide() - 10, 25)
	b:SetText("Save")
	b:Dock(TOP)
	b:DockMargin(5,0,5,5)
	b.DoClick = function(self)
		local num = tonumber(te:GetValue())
		if not num then return end
		net.Start("urp.properties.add")
		net.WriteString(te_n:GetValue())
		net.WriteInt(num, 32)
		net.WriteEntity(ent)
		net.SendToServer()
	end
end
