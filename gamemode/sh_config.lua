RED = Color(182, 80, 47)
GREEN = Color(141, 161, 88)
YELLOW = Color(220, 175, 95)
BLUE = Color(126, 170, 199)
PURPLE = Color(176, 101, 152)
CYAN = Color(141, 220, 217)
WHITE = Color(217, 217, 217)

urp.config.debug = true

urp.config.default_money = 5000
urp.config.default_bank_money = 500
urp.config.money_remove_timer = 0
urp.config.money_sign = "€"

urp.config.chars = urp.config.chars or {}
urp.config.chars.max_chars = 3
urp.config.chars.cam_positions = {
	{pos = Vector(-1066, -1178, 176), ang = Angle(15, -1, 0)},
	{pos = Vector(-665, -1182, 138), ang = Angle(4, -90, 0)},
	{pos = Vector(-400, -1187, 139), ang = Angle(1, -1, 0)},
	{pos = Vector(-225, -1221, 147), ang = Angle(-3, -14, 0)},
	{pos = Vector(-133, -1347, 148), ang = Angle(1, -90, 0)},
	{pos = Vector(-117, -1801, 153), ang = Angle(-7, -173, 0)},
	{pos = Vector(-134, -2056, 157), ang = Angle(-2, -95, 0)},
	{pos = Vector(29, -2139, 152), ang = Angle(1, 0, 0)},
	{pos = Vector(681, -2156, 153), ang = Angle(-1, 77, 0)},
	{pos = Vector(872, -2140, 137), ang = Angle(-45, 90, 0)},
	{pos = Vector(1430, -2136, 169), ang = Angle(1, -5, 0)},
	{pos = Vector(1864, -2119, 166), ang = Angle(-8, -58, 0)},
	{pos = Vector(1890, -1322, 176), ang = Angle(4, 97, 0)},
	{pos = Vector(2312, -1326, 165), ang = Angle(-17, 0, 0)},
	{pos = Vector(2637, -1304, 190), ang = Angle(7, -95, 0)},
	{pos = Vector(2915, -1294, 184), ang = Angle(9, -140, 0)},
	{pos = Vector(3365, -1307, 160), ang = Angle(1, -26, 0)},
	{pos = Vector(3421, -838, 149), ang = Angle(0, 91, 0)},
	{pos = Vector(3411, -446, 148), ang = Angle(-1, -26, 0)},
	{pos = Vector(3421, -183, 149), ang = Angle(0, 88, 0)},
	{pos = Vector(3432, 1257, 144), ang = Angle(0, 88, 0)},
	{pos = Vector(2708, 1340, 150), ang = Angle(2, -87, 0)},
	{pos = Vector(1830, 1329, 152), ang = Angle(1, -94, 0)},
	{pos = Vector(1652, 1392, 153), ang = Angle(1, 84, 0)},
	{pos = Vector(1689, 1434, 148), ang = Angle(19, 86, 0)},
	{pos = Vector(1698, 1548, 109), ang = Angle(26, 90, 0)},
	{pos = Vector(1697, 1747, 141), ang = Angle(-13, 91, 0)},
	{pos = Vector(1188, 1753, 169), ang = Angle(-1, -177, 0)},
	{pos = Vector(1137, 1721, 217), ang = Angle(-12, 92, 0)},
	{pos = Vector(1122, 1246, 151), ang = Angle(8, -90, 0)},
	{pos = Vector(1112, 407, 150), ang = Angle(2, -90, 0)},
	{pos = Vector(492, 194, 154), ang = Angle(1, -175, 0)},
	{pos = Vector(-156, -1, 144), ang = Angle(2, -110, 0)},
	{pos = Vector(-187, -479, 194), ang = Angle(-11, -14, 0)},
	{pos = Vector(-208, -923, 177), ang = Angle(7, -94, 0)},
	{pos = Vector(-168, -1095, 163), ang = Angle(6, -152, 0)},
}

urp.config.chars.models = {
	male = {
		["models/player/group01/male_01.mdl"] = true,
		["models/player/group01/male_02.mdl"] = true,
		["models/player/group01/male_03.mdl"] = true,
		["models/player/group01/male_04.mdl"] = true,
		["models/player/group01/male_05.mdl"] = true,
		["models/player/group01/male_06.mdl"] = true,
		["models/player/group01/male_07.mdl"] = true,
		["models/player/group01/male_08.mdl"] = true,
		["models/player/group01/male_09.mdl"] = true,
	},
	female = {
		["models/player/group01/female_01.mdl"] = true,
		["models/player/group01/female_02.mdl"] = true,
		["models/player/group01/female_03.mdl"] = true,
		["models/player/group01/female_04.mdl"] = true,
		["models/player/group01/female_05.mdl"] = true,
		["models/player/group01/female_06.mdl"] = true,
	}
}
