urp.commands = urp.commands or {}
urp.commands.list = urp.commands.list or {}

USER_ACCESS = 1
ADMIN_ACCESS = 2
SUPERADMIN_ACCESS = 3

function urp.commands.register(command, access, func, allias)
	urp.commands.list[command] = {
		access = access,
		func = func,
	}

	if not allias then return end

	for _,v in pairs(allias) do
		urp.commands.list[v] = urp.commands.list[command]
	end
end

local function concommand_add(ply, cmd, args)
	local command = args[1]
	local command_tbl = urp.commands.list[command]

	if not command_tbl then
		urp.notify(ply, 1, 4, "Команда не существует!")
		return
	end

	if not urp.commands.has_access(ply, command) then
		urp.notify(ply, 1, 4, "У вас нет доступа к этой команде!")
		return
	end

	table.remove(args, 1)
	command_tbl.func(ply, args)
end
concommand.Add("urp", concommand_add)

hook.Add("PlayerSay", "urp.commands.PlayerSay", function(ply, text)
	local args = string.Explode(" ", text)
	local command = string.gsub(args[1], "/", "", 1)

	if args[1] == "/" .. command and urp.commands.list[command] then
		args[1] = command
		concommand_add(ply, command, args)
		return ""
	end
end)

function urp.commands.has_access(ply, command)
	if urp.commands.list[command].access == ADMIN_ACCESS then
		return ply:IsAdmin()
	elseif urp.commands.list[command].access == SUPERADMIN_ACCESS then
		return ply:IsSuperAdmin()
	else
		return true
	end
end
