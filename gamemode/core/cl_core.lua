urp.colors = {
	URP = Color(220, 175, 95),
	Background = Color(0,0,0, 100),

}

local i = 1

local function add_debug_line(text)
	draw.SimpleText(text, urp.util.GetFont(12), 15, 20 * i, color_white)
	i = i + 1
end

hook.Add("HUDPaint", "urp.debug.HUDPaint", function()
	if not urp.config.debug then return end

	local ply = LocalPlayer()
	local name = ply:GetFullName()
	local money = ply:GetMoney()
	local bank_money = ply:GetBankMoney()

	add_debug_line("Name: " .. name)
	add_debug_line("Money: " .. urp.format_money(money))
	add_debug_line("Bank Money: " .. urp.format_money(bank_money))
	i = 1
end)

surface.CreateFont("urp.draw_overlay_text", { font = "Roboto Bold", size = 19, extended = true })

function urp.DrawOverlayText(ent)
	if not ent.markup_obj then
		ent.markup_obj = markup.Parse("<font=urp.draw_overlay_text>" .. ent.OverlayText .. "</font>")
	end
	local w, h = ent.markup_obj:GetWidth(), ent.markup_obj:GetHeight()
	local pos = (ent:GetPos() + Vector(0, 0, ent:OBBMaxs().z - 10) ):ToScreen()

	local x, y = math.floor(pos.x-w * .5), math.floor(pos.y-h-100)

	draw.RoundedBox(4, x, y, w + 20, h + 20, Color(0,0,0, 200))
	draw.SimpleText("u","marlett", x + w * .5 + 10, y + h + 13, Color(0,0,0, 200), TEXT_ALIGN_CENTER, TEXT_ALIGN_TOP)

	ent.markup_obj:Draw( x + 10, y + 10, TEXT_ALIGN_LEFT, TEXT_ALIGN_TOP, 255 )
end

net.Receive("urp.update_ent_text", function()
	local ent = net.ReadEntity()
	if not IsValid(ent) then return end
	ent.markup_obj = nil
	ent:UpdateOverlayText()
end)

hook.Add("HUDPaint", "ENT.OverlayText", function()
	for k,ent in pairs(urp.ents_list) do
		if not ent.OverlayText then continue end
		if not LocalPlayer():IsLineOfSightClear(ent:GetPos()) then continue end
		if ent:GetPos():DistToSqr(LocalPlayer():GetPos()) > 40000 then continue end

		urp.DrawOverlayText(ent)
	end

end)
