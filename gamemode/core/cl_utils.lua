local blur = Material("pp/blurscreen")
function urp.util.ScreenScale( size )
	return size * ( ScrH() / 480.0 )
end

function urp.util.DrawOutline(x, y, w, h, t, col)
	col = col or Color(0,0,0)
	surface.SetDrawColor(col.r, col.g, col.b, col.a)

	surface.DrawRect(x, y + h - t, w, t)
	surface.DrawRect(x, y + h - h, w, t)
	surface.DrawRect(x + w - t, y, t, h)
	surface.DrawRect(x + w - w, y, t, h)
end

function urp.util.DrawBlur(panel, amount)
	local x, y = panel:LocalToScreen(0, 0)
	local scrW, scrH = ScrW(), ScrH()

	surface.SetDrawColor(255, 255, 255)
	surface.SetMaterial(blur)
	for i = 1, 3 do
		blur:SetFloat("$blur", (i / 3) * (amount or 6))
		blur:Recompute()
		render.UpdateScreenEffectTexture()
		surface.DrawTexturedRect(x * -1, y * -1, scrW, scrH)
	end
end

function urp.util.DrawShadowText(text, font, x, y, color, x_a, y_a, color_shadow)
	color_shadow = color_shadow or Color(0, 0, 0,255)
	draw.SimpleText(text, font, x + 1, y + 1, color_shadow, x_a, y_a)
	local w,h = draw.SimpleText(text, font, x, y, color, x_a, y_a)
	return w,h
end

function urp.util.DrawBText(text, font, shadow_font, x, y, color, x_a, y_a, color_shadow)
	color_shadow = color_shadow or Color(0, 0, 0)
	draw.SimpleText(text, shadow_font, x + 1, y + 1, color_shadow, x_a, y_a)
	local w,h = draw.SimpleText(text, font, x, y, color, x_a, y_a)
	return w,h
end

function urp.util.DrawRect(x,y,w,h,col)
	col_o = col_o or Color(0,0,0,100)

	surface.SetDrawColor(col.r, col.g, col.b, col.a)
	surface.DrawRect(x,y,w,h)

	urp.util.DrawOutline(x, y, w, h, 1, col_o)
end

function urp.util.DrawBox(x,y,w,h,col,col_o)
	col_o = col_o or Color(0,0,0,100)
	col = col or Color(0,0,0,200)

	surface.SetDrawColor(col.r, col.g, col.b, col.a)
	surface.DrawRect(x,y,w,h)

	urp.util.DrawOutline(x, y, w, h, 1, col_o)
end

function urp.util.DrawBlurRect(x, y, w, h, col, col_o)
	col = col or Color(0,0,0,100)
	col_o = col_o or Color(0,0,0,100)

	local X, Y = 0,0

	surface.SetDrawColor(255,255,255, col.a)
	surface.SetMaterial(blur)

	for i = 1, 3 do
		blur:SetFloat("$blur", (i / 3) * (5))
		blur:Recompute()

		render.UpdateScreenEffectTexture()

		render.SetScissorRect(x, y, x+w, y+h, true)
			surface.DrawTexturedRect(X * -1, Y * -1, ScrW(), ScrH())
		render.SetScissorRect(0, 0, 0, 0, false)
	end

	urp.util.DrawBox(x,y,w,h,col, col_o)
end

function urp.util.GetFont(size)
	size = math.floor(math.Clamp(urp.util.ScreenScale(size), 1, 128))
	return "urp_font_" .. size
end

function urp.util.DrawCircle(x, y, radius, seg)
	local cir = {}

	table.insert(cir, {
		x = x,
		y = y
	})

	for i = 0, seg do
		local a = math.rad((i / seg) * -360)

		table.insert(cir, {
			x = x + math.sin(a) * radius,
			y = y + math.cos(a) * radius
		})
	end

	local a = math.rad(0)

	table.insert(cir, {
		x = x + math.sin(a) * radius,
		y = y + math.cos(a) * radius
	})

	surface.DrawPoly(cir)
end


if not urp.util.fonts_initialized then
	urp.util.fonts_initialized = true
	for i = 1, 128 do
		surface.CreateFont("urp_font_" .. i, {
			font = "Roboto",
			size = i,
			weight = 500,
			antialias = true,
			extended = true,
		})

		surface.CreateFont("urp_font_" .. i .. "_shadow", {
			font = "Roboto",
			size = i,
			weight = 500,
			antialias = true,
			extended = true,
			blursize = 8,
		})
	end
end

concommand.Add("_pos", function()
	local p = LocalPlayer():GetPos()
	local t = ("Vector(%d, %d, %d)"):format(math.Round(p.x), math.Round(p.y), math.Round(p.z))
	SetClipboardText(t)
	print(t)
	print("Done!")
end)

concommand.Add("_epos", function()
	local p = LocalPlayer():EyePos()
	local t = ("Vector(%d, %d, %d)"):format(math.Round(p.x), math.Round(p.y), math.Round(p.z))
	SetClipboardText(t)
	print(t)
	print("Done!")
end)

concommand.Add("_ang", function()
	local p = LocalPlayer():GetAngles()
	local t = ("Angle(%d, %d, %d)"):format(math.Round(p.x), math.Round(p.y), math.Round(p.z))
	SetClipboardText(t)
	print(t)
	print("Done!")
end)

local tb = {}
concommand.Add("_cfg_cam", function()
	local p = LocalPlayer():EyePos()
	local a = LocalPlayer():GetAngles()
	local t = ("{pos = Vector(%d, %d, %d), ang = Angle(%d, %d, %d)},"):format(math.Round(p.x), math.Round(p.y), math.Round(p.z), math.Round(a.x), math.Round(a.y), math.Round(a.z))
	local id = #tb + 1
	tb[id] = t
	print(id)
end)

concommand.Add("_cfg_cam_copy", function()
	local t = "{\n"
	for k,v in ipairs(tb) do
		t = t .. "	" .. v .. "\n"
	end
	t = t .. "}"
	SetClipboardText(t)
	print(t)
	print("Done!")
end)
