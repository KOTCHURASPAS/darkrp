hook.Add("DatabaseInitialized", "urp.DatabaseInitialized", function()
	local AUTOINCREMENT = MySQLite.isMySQL() and "AUTO_INCREMENT" or "AUTOINCREMENT"
	local LONGTEXT = MySQLite.isMySQL() and "LONGTEXT" or "TEXT"

	MySQLite.query([[ CREATE TABLE IF NOT EXISTS urp_characters (
		id INTEGER PRIMARY KEY ]] .. AUTOINCREMENT .. [[,
		steamid VARCHAR(255),
		steamid64 VARCHAR(255),
		first_name VARCHAR(255),
		last_name VARCHAR(255),
		inventory ]] .. LONGTEXT .. [[,
		money_held INTEGER,
		money_bank INTEGER,
		model VARCHAR(255),
		model_skin INTEGER,
		model_bgroups ]] .. LONGTEXT .. [[,
		vehicles ]] .. LONGTEXT .. [[,
		properties ]] .. LONGTEXT .. [[,
		inventory_bank ]] .. LONGTEXT .. [[,
		needs ]] .. LONGTEXT .. [[,
		skills ]] .. LONGTEXT .. [[
	); ]], nil, function(e) urp.print_debug("MYSQL", e) end)

	MySQLite.query([[ CREATE TABLE IF NOT EXISTS urp_properties (
		id INTEGER PRIMARY KEY ]] .. AUTOINCREMENT .. [[,
		map VARCHAR(255),
		name VARCHAR(255),
		cost INTEGER,
		doors ]] .. LONGTEXT .. [[,
		char_id INTEGER,
		FOREIGN KEY(char_id) REFERENCES urp_characters(id)
	);]], nil, function(e) urp.print_debug("MYSQL", e) end)

	MySQLite.query([[
		CREATE INDEX IF NOT EXISTS inx_char_steamid ON urp_characters(steamid);
		CREATE INDEX IF NOT EXISTS inx_char_steamid64 ON urp_characters(steamid64);
		CREATE INDEX IF NOT EXISTS inx_property_map ON urp_properties(map);
		CREATE INDEX IF NOT EXISTS inx_property_name ON urp_properties(name);
	]], nil, function(e) urp.print_debug("MYSQL", e) end)

	hook.Run("urp.database.loaded")
end)

util.AddNetworkString("urp.update_ent_text")
function urp.update_ent_text(ent, text)
	net.Start("urp.update_ent_text")
	net.WriteEntity(ent)
	net.WriteString(text)
	net.Broadcast()
end
